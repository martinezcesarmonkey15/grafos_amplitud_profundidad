/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica_java;

import java.util.ArrayList;

/**
 *
 * @author Monkeyelgrande
 */
public class Grafos_izquierda_derecha {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Izquierda
        int nodo = 0;
        System.out.println("Nodo de inicio: "+ nodo+"\n\n");
        
        Grafo g = new Grafo();
        ArrayList<Integer> enAnchuraIzquierda = g.recorridoAnchuraIzquierda(nodo);//Nodo inicial 0
        System.out.println("Recorrido en ANCHURA de un grafo representado como matriz de IZQUIERDA: ");
        System.out.println("" + enAnchuraIzquierda);
        for (int i = 0; i < enAnchuraIzquierda.size(); i++) {
            System.out.print("" + valorArray(enAnchuraIzquierda.get(i)) + "-");
        }
        System.out.println("");
        
        ArrayList<Integer> enProfundidadIzquierda = g.recorridoProfunidadIzquierda(nodo);//Nodo inicial 0
        System.out.println("Recorrido en PROFUNDIDAD de un grafo representado como matriz de IZQUIERDA: ");
        System.out.println("" + enProfundidadIzquierda);
        for (int i = 0; i < enProfundidadIzquierda.size(); i++) {
            System.out.print("" + valorArray(enProfundidadIzquierda.get(i)) + "-");
        }
        System.out.println("");
        System.out.println("");

        //DERECHA
        ArrayList<Integer> enAnchuraDerecha = g.recorridoAnchuraDerecha(nodo);//Nodo inicial 0
        System.out.println("Recorrido en ANCHURA de un grafo representado como matriz de DERECHA: ");
        System.out.println("" + enAnchuraDerecha);
        for (int i = 0; i < enAnchuraDerecha.size(); i++) {
            System.out.print("" + valorArray(enAnchuraDerecha.get(i)) + "-");
        }
        System.out.println("");
        
        
        
        ArrayList<Integer> enProfundidadDerecha = g.recorridoProfunidadDerecha(nodo);//Nodo inicial 0
        System.out.println("Recorrido en PROFUNDIDAD de un grafo representado como matriz de DERECHA: ");
        System.out.println("" + enProfundidadDerecha);

        for (int i = 0; i < enProfundidadDerecha.size(); i++) {
            System.out.print("" + valorArray(enProfundidadDerecha.get(i)) + "-");
        }
        System.out.println("");

    }

    public static String valorArray(int i) {
        String letras[] = {"J","K","D","M","C","L","A","H","O","I","B","N","Q","R","G","F","P","E"};
        return letras[i];
    }
}
