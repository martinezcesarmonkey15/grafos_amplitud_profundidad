/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package practica_java;

/**
 *
 * @author UTS
 */
import java.util.ArrayList;

/**
 * Clase Grafo
 *
 * @author UTS
 */
public class Grafo {

    public int[][] g = {
        {2, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 2, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 0, 2, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 0, 0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 1, 0, 0, 2, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0},
        {0, 0, 1, 0, 0, 2, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
        {0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
        {0, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 1, 1, 0},
        {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 1},
        {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2}

    };
    private boolean[] visitiadoAnchuraIzquierda = new boolean[18];
    private boolean[] visitiadoProfunidadIzquierda = new boolean[18];

    private boolean[] visitiadoAnchuraDerecha = new boolean[18];
    private boolean[] visitiadoProfunidadDerecha = new boolean[18];

    public Grafo() {
    }

    public int[][] getG() {
        return g;
    }

    public ArrayList<Integer> recorridoAnchuraIzquierda(int nodoI) {
        //Lista donde guardo los nodos recorridos
        ArrayList<Integer> recorridos = new ArrayList<Integer>();
        //El nodo inicial ya está visitado
        visitiadoAnchuraIzquierda[nodoI] = true;
        //Cola de visitas de los nodos adyacentes
        ArrayList<Integer> cola = new ArrayList<Integer>();
        //Se lista el nodo como ya recorrido
        recorridos.add(nodoI);
        //Se agrega el nodo a la cola de visitas
        cola.add(nodoI);
        //Hasta que visite todos los nodos
        while (!cola.isEmpty()) {
            int j = cola.remove(0); //Se saca el primero nodo de la cola
            //Se busca en la matriz que representa el grafo los nodos adyacentes
            for (int i = 0; i < g.length; i++) {
                //Si es un nodo adyacente y no está visitado entonces
                if (g[j][i] == 1 && !visitiadoAnchuraIzquierda[i]) {
                    cola.add(i);//Se agrega a la cola de visitas
                    recorridos.add(i);//Se marca como recorrido
                    visitiadoAnchuraIzquierda[i] = true;//Se marca como visitado
                }
            }
        }
        return recorridos;//Devuelvo el recorrido del grafo en anchura
    }

    public ArrayList<Integer> recorridoProfunidadIzquierda(int nodoI) {
//Lista donde guardo los nodos recorridos
        ArrayList<Integer> recorridos = new ArrayList<Integer>();
        visitiadoProfunidadIzquierda[nodoI] = true;//El nodo inicial ya está visitado
//Cola de visitas de los nodos adyacentes
        ArrayList<Integer> cola = new ArrayList<Integer>();
        recorridos.add(nodoI);//Listo el nodo como ya recorrido
        cola.add(nodoI);//Se agrega el nodo a la cola de visitas
        while (!cola.isEmpty()) {//Hasta que visite todos los nodos
            int j = cola.remove(0);//Se saca el primer nodo de la cola
            //Se busca en la matriz que representa el grafo los nodos adyacentes
            for (int i = 0; i < g.length; i++) {
//Si es un nodo adyacente y no está visitado entonces
                if (g[j][i] == 1 && !visitiadoProfunidadIzquierda[i]) {
                    cola.add(i);//Se agrega a la cola de visita
//Se recorren los hijos del nodo actual de visita y se agrega el recorrido al la lista
                    recorridos.addAll(recorridoProfunidadIzquierda(i));
                    visitiadoProfunidadIzquierda[i] = true;//Se marca como visitado
                }
            }
        }
        return recorridos;//Se devuelve el recorrido del grafo en profundidad
    }

    public ArrayList<Integer> recorridoAnchuraDerecha(int nodoI) {
        //Lista donde guardo los nodos recorridos
        ArrayList<Integer> recorridos = new ArrayList<Integer>();
        //El nodo inicial ya está visitado
        visitiadoAnchuraDerecha[nodoI] = true;
        //Cola de visitas de los nodos adyacentes
        ArrayList<Integer> cola = new ArrayList<Integer>();
        //Se lista el nodo como ya recorrido
        recorridos.add(nodoI);
        //Se agrega el nodo a la cola de visitas
        cola.add(nodoI);
        //Hasta que visite todos los nodos
        int g_length = g.length - 1;
        while (!cola.isEmpty()) {
            int j = cola.remove(0); //Se saca el primero nodo de la cola
            //Se busca en la matriz que representa el grafo los nodos adyacentes
            for (int i = 0; i < g.length; i++) {
                //Si es un nodo adyacente y no está visitado entonces
                if (g[j][g_length - i] == 1 && !visitiadoAnchuraDerecha[g_length - i]) {
                    cola.add(g_length - i);//Se agrega a la cola de visitas
                    recorridos.add(g_length - i);//Se marca como recorrido
                    visitiadoAnchuraDerecha[g_length - i] = true;//Se marca como visitado
                }
            }
        }
        return recorridos;//Devuelvo el recorrido del grafo en anchura
    }

    public ArrayList<Integer> recorridoProfunidadDerecha(int nodoI) {
//Lista donde guardo los nodos recorridos
        ArrayList<Integer> recorridos = new ArrayList<Integer>();
        visitiadoProfunidadDerecha[nodoI] = true;//El nodo inicial ya está visitado
//Cola de visitas de los nodos adyacentes
        ArrayList<Integer> cola = new ArrayList<Integer>();
        recorridos.add(nodoI);//Listo el nodo como ya recorrido
        cola.add(nodoI);//Se agrega el nodo a la cola de visitas
        int g_length = g.length - 1;

        while (!cola.isEmpty()) {//Hasta que visite todos los nodos
            int j = cola.remove(0);//Se saca el primer nodo de la cola
            //Se busca en la matriz que representa el grafo los nodos adyacentes
            for (int i = 0; i < g.length; i++) {
                //Si es un nodo adyacente y no está visitado entonces
                if (g[j][g_length - i] == 1 && !visitiadoProfunidadDerecha[g_length - i]) {
                    cola.add(g.length -1 - i);//Se agrega a la cola de visita
                    //Se recorren los hijos del nodo actual de visita y se agrega el recorrido al la lista
                    recorridos.addAll(recorridoProfunidadDerecha(g_length - i));
                    visitiadoProfunidadDerecha[g_length - i] = true;//Se marca como visitado
                }
            }
        }
        return recorridos;//Se devuelve el recorrido del grafo en profundidad
    }
}
